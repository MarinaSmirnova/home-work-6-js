function createNewUser() {
    const newUser = {
        get getLogin() {
            return (this._firstName[0] + this._lastName).toLowerCase();
        },

        get getAge() {
            let now = new Date;
            let age = Math.floor((now - this._birthday) / 1000 / 3600 / 24 / 365.25);
            return age;
        },

        get getPassword() {
            let password = (this._firstName[0]).toUpperCase() + this._lastName.toLowerCase() + this._birthday.getFullYear();
            return password;
        },

        set firstName(value) {
            this._firstName = value;
        },

        set lastName(value) {
            this._lastName = value;
        },

        set birthday(value) {
            this._birthday = new Date(value.substring(6), ((value.substring(3, 5)) - 1), value.substring(0, 2));
        }
    };

    return newUser;
}

const user = createNewUser();

user.firstName = prompt("Enter your first name");
user.lastName = prompt("Enter your last name");

user.birthday = prompt("Enter your birthday");

console.log(user.getLogin);

console.log("Your age is " + user.getAge);

console.log("Your password is " + user.getPassword);